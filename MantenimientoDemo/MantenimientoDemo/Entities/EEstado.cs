﻿namespace MantenimientoDemo.Entities
{
    public class EEstado
    {
        public int Id { get; set; }
        public string Estado { get; set; }
        public EGenero Genero { get; set; }
        public bool activo { get; set; }
    }
}