﻿using System.Drawing;

namespace MantenimientoDemo.Entities
{
    public class EImagen
    {
        public int Id { get; set; }
        public Image Imagen { get; set; }
    }
}