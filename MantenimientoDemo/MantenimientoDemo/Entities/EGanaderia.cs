﻿namespace MantenimientoDemo.Entities
{
    public class EGanaderia
    {
        public int Id { get; set; }
        public string  Nombre { get; set; }
        public bool Activo { get; set; }
        public EImagen Sello { get; set; }

        public override string ToString()
        {
            return Nombre;
        }
    }
}