﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MantenimientoDemo.Entities
{
    public class EBovino
    {
        public int Id { get; set; }
        public int Numero { get; set; }
        public EGanaderia Ganaderia { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public EGenero Genero { get; set; }
        public EBovino Mama { get; set; }
        public EEstado Estado { get; set; }
        public EImagen Foto { get; set; }

        public bool Activo { get; set; }

    }
}
