﻿using MantenimientoDemo.DAL;
using MantenimientoDemo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MantenimientoDemo.BOL
{
    class BovinoBOL
    {
        internal List<EBovino> CargarBovinos(string filtro)
        {
            return new BovinoDAL().CargarBovinos(filtro);
        }
    }
}
