﻿using System;
using MantenimientoDemo.Entities;
using Npgsql;

namespace MantenimientoDemo.DAL
{
    internal class GanaderiaDAL
    {
        public GanaderiaDAL()
        {
        }

        internal EGanaderia CargarGanaderia(int id)
        {
            Console.WriteLine(id);
            string sql = "select id, nombre, id_imagen, activo from ganaderia.ganaderias where id = @id";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarGanadería(reader);
                }
            }
            return null;
        }

        private EGanaderia CargarGanadería(NpgsqlDataReader reader)
        {
            EGanaderia g = new EGanaderia
            {
                Id = reader.GetInt32(0),
                Nombre = reader.GetString(1),
                Sello = null
            };
            return g;
        }
    }
}