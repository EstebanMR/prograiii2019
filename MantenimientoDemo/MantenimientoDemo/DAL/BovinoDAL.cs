﻿using MantenimientoDemo.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MantenimientoDemo.DAL
{
    class BovinoDAL
    {
        internal List<EBovino> CargarBovinos(string filtro)
        {
            List<EBovino> bovinos = new List<EBovino>();
            bovinos.Add(new EBovino { Id = 1, Numero = 12312, Activo = true });
            string sql = "select id, numero, id_bovino, id_ganaderia, fecha_nac, id_imagen, id_genero, id_estado, activo" +
                " from ganaderia.bovinos";
            sql += !String.IsNullOrEmpty(filtro) ? " where numero like @fil" : "";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                if (!String.IsNullOrEmpty(filtro))
                {
                    cmd.Parameters.AddWithValue("@fil", filtro);
                }
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    bovinos.Add(CargarBovinos(reader));
                }
            }
            return bovinos;
        }

        private EBovino CargarBovinos(NpgsqlDataReader reader)
        {
            EBovino temp = new EBovino
            {
                Id = reader.GetInt32(0),
                Numero = reader.GetInt32(1),
                Mama = !reader.IsDBNull(2) ? CargarBovinos(reader.GetInt32(2)) : null,
                Ganaderia = !reader.IsDBNull(3) ? new GanaderiaDAL().CargarGanaderia(reader.GetInt32(2)) : null,
                FechaNacimiento = reader.GetDateTime(4),
                Foto = !reader.IsDBNull(4) ? new ImagenDAL().CargarImagen(reader.GetInt32(4)) : null,
                Genero = !reader.IsDBNull(5) ? new GeneroDAL().CargarGenero(reader.GetInt32(5)) : null,
                Estado = !reader.IsDBNull(6) ? new EstadoDAL().CargarEstado(reader.GetInt32(5)) : null,
                Activo = reader.GetBoolean(7)
            };
            return temp;
        }

        private EBovino CargarBovinos(int id)
        {
            string sql = "select id, numero, id_bovino, id_ganaderia, fecha_nac, id_imagen, id_genero, id_estado, activo from ";
            return null;
        }
    }
}
