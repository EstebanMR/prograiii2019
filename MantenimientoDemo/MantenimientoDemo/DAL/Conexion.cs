﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MantenimientoDemo.DAL
{
    class Conexion
    {
        public static string conStr = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;
        
        public void Test()
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                MessageBox.Show("Conexión exitosa");
            }
        }
    }

    
}
