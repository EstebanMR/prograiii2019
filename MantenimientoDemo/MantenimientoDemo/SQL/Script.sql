CREATE TABLE ganaderia.ganaderias
(
	id SERIAL PRIMARY KEY,
	nombre TEXT NOT NULL UNIQUE,
	sello INT NOT NULL,
	activo BOOLEAN DEFAULT TRUE,
	CONSTRAINT fk_gan_img FOREIGN KEY(sello)
		REFERENCES ganaderia.imagenes(id)
);

CREATE TABLE ganaderia.imagenes
(
	id SERIAL PRIMARY KEY,
	imagen BYTEA,
	activo BOOLEAN DEFAULT TRUE
);

CREATE TABLE ganaderia.bovinos
(
	id SERIAL PRIMARY KEY,
	numero INT NOT NULL unique,
	id_bovino INT,
	id_ganaderia INT NOT NULL,
	fecha_nac TIMESTAMP NOT NULL,
	id_imagen INT,
	id_genero INT NOT NULL,
	id_estado INT NOT NULL,
	activo BOOLEAN DEFAULT TRUE,
	CONSTRAINT fk_bov_bov FOREIGN KEY(id_bovino)
		REFERENCES ganaderia.bovinos(id),
	CONSTRAINT fk_bov_gan FOREIGN KEY(id_ganaderia)
		REFERENCES ganaderia.ganaderias(id),
	CONSTRAINT fk_bov_img FOREIGN KEY(id_imagen)
		REFERENCES ganaderia.imagenes(id),
	CONSTRAINT fk_gen FOREIGN KEY(id_genero)
		REFERENCES ganaderia.generos(id),
	CONSTRAINT fk_bov_est FOREIGN KEY(id_estado)
		REFERENCES ganaderia.estados(id)
);

CREATE TABLE ganaderia.estados
(
	id SERIAL PRIMARY KEY,
	estado TEXT NOT NULL UNIQUE,
	id_genero INT NOT NULL,
	activo BOOLEAN DEFAULT TRUE,
	CONSTRAINT fk_est_gen FOREIGN KEY(id_genero)
		REFERENCES ganaderia.generos(id)
	
);

CREATE TABLE ganaderia.generos
(
	id SERIAL PRIMARY KEY,
	genero TEXT NOT NULL UNIQUE,
	simbolo CHAR NOT NULL,
	activo BOOLEAN DEFAULT TRUE
);