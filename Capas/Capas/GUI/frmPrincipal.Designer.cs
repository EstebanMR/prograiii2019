﻿namespace Capas.GUI
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.gbJug1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTiros1 = new System.Windows.Forms.TextBox();
            this.txtPunt1 = new System.Windows.Forms.TextBox();
            this.gbJug2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTiros2 = new System.Windows.Forms.TextBox();
            this.txtPunt2 = new System.Windows.Forms.TextBox();
            this.gbJuego = new System.Windows.Forms.GroupBox();
            this.txtRes = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pbx2 = new System.Windows.Forms.PictureBox();
            this.pbx1 = new System.Windows.Forms.PictureBox();
            this.btnLanzar = new System.Windows.Forms.Button();
            this.txtNum = new System.Windows.Forms.TextBox();
            this.gbJug1.SuspendLayout();
            this.gbJug2.SuspendLayout();
            this.gbJuego.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx1)).BeginInit();
            this.SuspendLayout();
            // 
            // gbJug1
            // 
            this.gbJug1.BackColor = System.Drawing.SystemColors.Highlight;
            this.gbJug1.Controls.Add(this.label4);
            this.gbJug1.Controls.Add(this.label3);
            this.gbJug1.Controls.Add(this.txtTiros1);
            this.gbJug1.Controls.Add(this.txtPunt1);
            this.gbJug1.Location = new System.Drawing.Point(12, 12);
            this.gbJug1.Name = "gbJug1";
            this.gbJug1.Size = new System.Drawing.Size(389, 134);
            this.gbJug1.TabIndex = 0;
            this.gbJug1.TabStop = false;
            this.gbJug1.Text = "Jugador #1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(234, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 25);
            this.label4.TabIndex = 10;
            this.label4.Text = "Tiros:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 25);
            this.label3.TabIndex = 9;
            this.label3.Text = "Puntaje:";
            // 
            // txtTiros1
            // 
            this.txtTiros1.Font = new System.Drawing.Font("Agency FB", 35.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTiros1.Location = new System.Drawing.Point(300, 19);
            this.txtTiros1.Name = "txtTiros1";
            this.txtTiros1.ReadOnly = true;
            this.txtTiros1.Size = new System.Drawing.Size(83, 63);
            this.txtTiros1.TabIndex = 8;
            this.txtTiros1.Text = "2/10";
            this.txtTiros1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPunt1
            // 
            this.txtPunt1.Font = new System.Drawing.Font("Agency FB", 35.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPunt1.Location = new System.Drawing.Point(118, 57);
            this.txtPunt1.Name = "txtPunt1";
            this.txtPunt1.ReadOnly = true;
            this.txtPunt1.Size = new System.Drawing.Size(64, 63);
            this.txtPunt1.TabIndex = 7;
            this.txtPunt1.Text = "95";
            this.txtPunt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gbJug2
            // 
            this.gbJug2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbJug2.Controls.Add(this.label6);
            this.gbJug2.Controls.Add(this.label5);
            this.gbJug2.Controls.Add(this.txtTiros2);
            this.gbJug2.Controls.Add(this.txtPunt2);
            this.gbJug2.Location = new System.Drawing.Point(418, 12);
            this.gbJug2.Name = "gbJug2";
            this.gbJug2.Size = new System.Drawing.Size(396, 134);
            this.gbJug2.TabIndex = 1;
            this.gbJug2.TabStop = false;
            this.gbJug2.Text = "Jugador #2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 25);
            this.label6.TabIndex = 12;
            this.label6.Text = "Puntaje:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(230, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 25);
            this.label5.TabIndex = 11;
            this.label5.Text = "Tiros:";
            // 
            // txtTiros2
            // 
            this.txtTiros2.Font = new System.Drawing.Font("Agency FB", 35.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTiros2.Location = new System.Drawing.Point(302, 11);
            this.txtTiros2.Name = "txtTiros2";
            this.txtTiros2.ReadOnly = true;
            this.txtTiros2.Size = new System.Drawing.Size(88, 63);
            this.txtTiros2.TabIndex = 8;
            this.txtTiros2.Text = "2/10";
            this.txtTiros2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPunt2
            // 
            this.txtPunt2.Font = new System.Drawing.Font("Agency FB", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPunt2.Location = new System.Drawing.Point(116, 57);
            this.txtPunt2.Name = "txtPunt2";
            this.txtPunt2.ReadOnly = true;
            this.txtPunt2.Size = new System.Drawing.Size(64, 55);
            this.txtPunt2.TabIndex = 7;
            this.txtPunt2.Text = "95";
            this.txtPunt2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gbJuego
            // 
            this.gbJuego.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbJuego.BackColor = System.Drawing.SystemColors.Control;
            this.gbJuego.Controls.Add(this.txtRes);
            this.gbJuego.Controls.Add(this.label2);
            this.gbJuego.Controls.Add(this.label1);
            this.gbJuego.Controls.Add(this.pbx2);
            this.gbJuego.Controls.Add(this.pbx1);
            this.gbJuego.Controls.Add(this.btnLanzar);
            this.gbJuego.Controls.Add(this.txtNum);
            this.gbJuego.Location = new System.Drawing.Point(12, 152);
            this.gbJuego.Name = "gbJuego";
            this.gbJuego.Size = new System.Drawing.Size(802, 286);
            this.gbJuego.TabIndex = 2;
            this.gbJuego.TabStop = false;
            this.gbJuego.Text = "Panel de Juego";
            // 
            // txtRes
            // 
            this.txtRes.Font = new System.Drawing.Font("Agency FB", 35.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRes.Location = new System.Drawing.Point(621, 112);
            this.txtRes.Name = "txtRes";
            this.txtRes.ReadOnly = true;
            this.txtRes.Size = new System.Drawing.Size(64, 63);
            this.txtRes.TabIndex = 6;
            this.txtRes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(346, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 46);
            this.label2.TabIndex = 5;
            this.label2.Text = "+";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(572, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 46);
            this.label1.TabIndex = 4;
            this.label1.Text = "=";
            // 
            // pbx2
            // 
            this.pbx2.Image = ((System.Drawing.Image)(resources.GetObject("pbx2.Image")));
            this.pbx2.Location = new System.Drawing.Point(406, 64);
            this.pbx2.Name = "pbx2";
            this.pbx2.Size = new System.Drawing.Size(141, 141);
            this.pbx2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx2.TabIndex = 3;
            this.pbx2.TabStop = false;
            // 
            // pbx1
            // 
            this.pbx1.Image = ((System.Drawing.Image)(resources.GetObject("pbx1.Image")));
            this.pbx1.Location = new System.Drawing.Point(176, 64);
            this.pbx1.Name = "pbx1";
            this.pbx1.Size = new System.Drawing.Size(141, 141);
            this.pbx1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx1.TabIndex = 2;
            this.pbx1.TabStop = false;
            // 
            // btnLanzar
            // 
            this.btnLanzar.Enabled = false;
            this.btnLanzar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnLanzar.Location = new System.Drawing.Point(6, 159);
            this.btnLanzar.Name = "btnLanzar";
            this.btnLanzar.Size = new System.Drawing.Size(140, 57);
            this.btnLanzar.TabIndex = 1;
            this.btnLanzar.Text = "Lanzar Dados";
            this.btnLanzar.UseVisualStyleBackColor = true;
            this.btnLanzar.Click += new System.EventHandler(this.btnLanzar_Click);
            // 
            // txtNum
            // 
            this.txtNum.Font = new System.Drawing.Font("Agency FB", 35.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNum.Location = new System.Drawing.Point(44, 55);
            this.txtNum.Name = "txtNum";
            this.txtNum.Size = new System.Drawing.Size(64, 63);
            this.txtNum.TabIndex = 0;
            this.txtNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNum.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtNum_MouseClick);
            this.txtNum.TextChanged += new System.EventHandler(this.TxtNum_TextChanged);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 450);
            this.Controls.Add(this.gbJuego);
            this.Controls.Add(this.gbJug2);
            this.Controls.Add(this.gbJug1);
            this.Name = "frmPrincipal";
            this.Text = "frmPrincipal";
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.gbJug1.ResumeLayout(false);
            this.gbJug1.PerformLayout();
            this.gbJug2.ResumeLayout(false);
            this.gbJug2.PerformLayout();
            this.gbJuego.ResumeLayout(false);
            this.gbJuego.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbx1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbJug1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTiros1;
        private System.Windows.Forms.TextBox txtPunt1;
        private System.Windows.Forms.GroupBox gbJug2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTiros2;
        private System.Windows.Forms.TextBox txtPunt2;
        private System.Windows.Forms.GroupBox gbJuego;
        private System.Windows.Forms.TextBox txtRes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbx2;
        private System.Windows.Forms.PictureBox pbx1;
        private System.Windows.Forms.Button btnLanzar;
        private System.Windows.Forms.TextBox txtNum;
    }
}