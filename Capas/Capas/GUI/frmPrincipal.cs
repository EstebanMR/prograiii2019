﻿using Capas.BOL;
using Capas.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capas.GUI
{
    public partial class frmPrincipal : Form
    {
        internal LogicaBOL log;
        public frmPrincipal()
        {
            InitializeComponent();

        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            log = new LogicaBOL();
            ValidarUsuarios();
        }

        private void ValidarUsuarios()
        {
            if (log.Jugador1 == null || log.Jugador2 == null)
            {
                frmRegistro frm = new frmRegistro { logica = log };
                Hide();
                frm.ShowDialog();
                Iniciar();
                Show();
            }
        }

        private void Iniciar()
        {
            gbJug1.Text = log.Jugador1?.Nombre;
            gbJug2.Text = log.Jugador2?.Nombre;
            log.Rifar();
            Pintar();
        }

        private void Pintar()
        {
            txtPunt1.Text = log.Jugador1?.PuntajeActual.Puntaje.ToString();
            txtPunt2.Text = log.Jugador2?.PuntajeActual.Puntaje.ToString();
            txtTiros1.Text = log.Jugador1?.PuntajeActual.Lanzamientos.ToString() + "/" + LogicaBOL.MAX_INT;
            txtTiros2.Text = log.Jugador2?.PuntajeActual.Lanzamientos.ToString() + "/" + LogicaBOL.MAX_INT;

            gbJug1.BackColor = log.Jugador1 == log.actual ? SystemColors.ActiveCaption : SystemColors.Window;
            gbJug2.BackColor = log.Jugador2 == log.actual ? SystemColors.ActiveCaption : SystemColors.Window;

        }

        private void TxtNum_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtNum.Text.Trim().Length > 0)
                {
                    int txt = int.Parse(txtNum.Text);
                    btnLanzar.Enabled = txt >= 2 && txt <= 12;
                }
                else
                {
                    btnLanzar.Enabled = false;
                }

            }
            catch (Exception)
            {

                btnLanzar.Enabled = false;
                txtNum.Text = "";
            }
        }

        private void btnLanzar_Click(object sender, EventArgs e)
        {
            btnLanzar.Enabled = false;
            Random r = new Random();
            int d1 = 0;
            int d2 = 0;
            for (int i = 0; i < 10; i++)
            {
                d1 = r.Next(1, 7);
                d2 = r.Next(1, 7);
                pbx1.Image = (Image)Resources.ResourceManager.GetObject("D" + d1);
                pbx2.Image = (Image)Resources.ResourceManager.GetObject("D" + d2);
                Refresh();
                Thread.Sleep(20 * i);
            }
            int num = int.Parse(txtNum.Text.Trim());
            txtRes.Text = d1 + d2 + "";
            log.Revisar(d1, d2, num);
            Pintar();
            Termino();
        }

        private void Termino()
        {
            if (log.Jugador1.PuntajeActual.Lanzamientos == LogicaBOL.MAX_INT
                && log.Jugador2.PuntajeActual.Lanzamientos==LogicaBOL.MAX_INT)
            {
                log.RegistrarPuntaje();
                log.Jugador1 = null;
                log.Jugador2 = null;
            }
            else if (log.DiferenciaAbismal())
            {
                log.RegistrarPuntaje();
                log.Jugador1 = null;
                log.Jugador2 = null;
            }
            ValidarUsuarios();
        }

        private void txtNum_MouseClick(object sender, MouseEventArgs e)
        {
            txtNum.Text = "";
        }

    }
}
