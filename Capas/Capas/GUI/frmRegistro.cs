﻿using Capas.BOL;
using Capas.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Capas.GUI
{
    public partial class frmRegistro : Form
    {
        internal LogicaBOL logica { get; set; }

        public frmRegistro()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            logica.Jugador1 = new EJugador
            {
                Id = logica.Jugador1 != null ? logica.Jugador1.Id : 0,
                Activo = true,
                Apellido1 = txtApellido.Text.Trim(),
                Correo = txtCorreo.Text.Trim(),
                Nombre = txtNombre.Text.Trim(),
                Pin = Int32.Parse(txtPin.Text)
            };
            logica.Registrar(logica.Jugador1);

            btnAceptar1.Enabled = false;
            if (!btnAceptar1.Enabled && !btnAceptar2.Enabled)
            {
                Close();
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            logica.Jugador2 = new EJugador
            {
                Id = logica.Jugador1 != null ? logica.Jugador1.Id : 0,
                Activo = true,
                Apellido1 = txtApellido1.Text.Trim(),
                Correo = txtCorreo1.Text.Trim(),
                Nombre = txtNombre1.Text.Trim(),
                Pin = Int32.Parse(txtPin1.Text)
            };
            logica.Registrar(logica.Jugador2);
            btnAceptar2.Enabled = false;
            if (!btnAceptar1.Enabled && !btnAceptar2.Enabled)
            {
                Close();
            }
        }

        private void TxtPin_TextChanged(object sender, EventArgs e)
        {
            btnAceptar1.Enabled = true;
        }

        private void TxtPin1_TextChanged(object sender, EventArgs e)
        {
            btnAceptar2.Enabled = true;
        }

        private void BtnCargar1_Click(object sender, EventArgs e)
        {
            logica.Jugador1 = new EJugador
            {
                Correo = txtCorreo.Text.Trim(),
                Pin = Int32.Parse(txtPin.Text)
            };
            if (logica.CargarJugador(logica.Jugador1))
            {
                txtNombre.Text = logica.Jugador1.Nombre;
                txtApellido.Text = logica.Jugador1.Apellido1;
                btnAceptar1.Enabled = false;
                if (!btnAceptar1.Enabled && !btnAceptar2.Enabled)
                {
                    Close();
                }
            }
        }

        private void BtnCargar2_Click(object sender, EventArgs e)
        {
            logica.Jugador2 = new EJugador
            {
                Correo = txtCorreo1.Text.Trim(),
                Pin = Int32.Parse(txtPin1.Text)
            };
            if (logica.CargarJugador(logica.Jugador2))
            {
                txtNombre1.Text = logica.Jugador2.Nombre;
                txtApellido1.Text = logica.Jugador2.Apellido1;
                btnAceptar2.Enabled = false;
                if (!btnAceptar1.Enabled && !btnAceptar2.Enabled)
                {
                    Close();
                }
            }
        }

        private void frmRegistro_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
