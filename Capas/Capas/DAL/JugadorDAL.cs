﻿using Capas.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capas.DAL
{
    class JugadorDAL
    {
        public void Insertar(EJugador jug)
        {
            string sql = "INSERT INTO dados.jugadores(correo, nombre, apellidos, pin) " +
                " VALUES(@cor, @nom, @ape, @pin); ";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexión.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@cor", jug.Correo);
                cmd.Parameters.AddWithValue("@nom", jug.Nombre);
                cmd.Parameters.AddWithValue("@ape", jug.Apellido1);
                cmd.Parameters.AddWithValue("@pin", jug.Pin);
                try 
                { 
                jug.Id = (int)cmd.ExecuteScalar();
                }catch (Exception r)
                {
                    Console.WriteLine(r.ToString());
                }
            }
        }

        internal void Actualizar(EJugador jug)
        {
            string sql = "UPDATE dados.jugadores SET correo =@cor, nombre =@nom, " +
                 "apellidos =@ape, pin =@pin, activo =@act WHERE id =@id";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexión.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@cor", jug.Correo);
                cmd.Parameters.AddWithValue("@nom", jug.Nombre);
                cmd.Parameters.AddWithValue("@ape", jug.Apellido1);
                cmd.Parameters.AddWithValue("@pin", jug.Pin);
                cmd.Parameters.AddWithValue("@act", jug.Activo);
                cmd.Parameters.AddWithValue("@id", jug.Id);
                cmd.ExecuteNonQuery();
            }

        }

        internal bool Cargar(EJugador jug)
        {
            string sql = "SELECT id, correo, nombre, apellidos, pin, activo " +
                " FROM dados.jugadores WHERE correo = @cor and pin = @pin";

            using (NpgsqlConnection con = new NpgsqlConnection(Conexión.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@cor", jug.Correo);
                cmd.Parameters.AddWithValue("@pin", jug.Pin);

                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    EJugador temp = Cargar(reader);
                    jug.Nombre = temp.Nombre;
                    jug.Apellido1 = temp.Apellido1;
                    jug.Activo = temp.Activo;
                    jug.Id = temp.Id;
                    Console.WriteLine(jug.Nombre);
                    return true;
                }
            }
            return false;

        }

        internal void RegistrarPuntaje(EJugador jug)
        {
            string sql = "INSERT INTO dados.puntajes " +
                "(id_jugador, puntaje, lanzamientos)" +
                " VALUES(@idj, @pun, @lan)";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexión.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@idj", jug.Id);
                cmd.Parameters.AddWithValue("@pun", jug.PuntajeActual.Puntaje);
                cmd.Parameters.AddWithValue("@lan", jug.PuntajeActual.Lanzamientos);
                cmd.ExecuteNonQuery();
            }
        }

        private EJugador Cargar(NpgsqlDataReader reader)
        {
            EJugador jug = new EJugador
            {
                Id = reader.GetInt32(0),
                Correo = reader.GetString(1),
                Nombre = reader.GetString(2),
                Apellido1 = reader.GetString(3),
                Pin = reader.GetInt32(4),
                Activo = reader.GetBoolean(5)
            };
            return jug;

        }
    }
}
