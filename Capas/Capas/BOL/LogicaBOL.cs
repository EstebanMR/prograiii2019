﻿using Capas.DAL;
using Capas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capas.BOL
{
    class LogicaBOL
    {
        public EJugador actual { get; set; }
        public EJugador Jugador1 { get; set; }
        public EJugador Jugador2 { get; set; }

        public const int MAX_INT = 10;

        public void Registrar(EJugador jugador)
        {
            Validar(jugador);
            if (jugador.Id > 0)
            {
                new JugadorDAL().Actualizar(jugador);
            }
            else
            {
                new JugadorDAL().Insertar(jugador);
                Console.WriteLine(jugador.Id);
            }

        }

        private void Validar(EJugador jugador)
        {
            if (string.IsNullOrWhiteSpace(jugador.Nombre))
            {
                throw new Exception("Nombre Requerido");
            }
            if (string.IsNullOrWhiteSpace(jugador.Correo))
            {
                throw new Exception("Correo Requerido");
            }

        }

        internal void Rifar()
        {
            Random r = new Random();
            int ran = r.Next(20);
            actual = ran <= 10 ? Jugador1 : Jugador2;
        }

        public bool CargarJugador(EJugador jugador)
        {
            if (string.IsNullOrWhiteSpace(jugador.Correo))
            {
                throw new Exception("Correo Requerido");
            }
            return new JugadorDAL().Cargar(jugador);

        }

        internal void Revisar(int d1, int d2, int num)
        {
            actual.PuntajeActual.Lanzamientos++;
            int sumD = d1 + d2;
            if (num == sumD)
            {
                actual.PuntajeActual.Puntaje += 2;
            }
            else if (num == sumD+1||num==sumD-1)
            {
                actual.PuntajeActual.Puntaje++;
            }

            actual = actual == Jugador1 ? Jugador2 : Jugador1;
        }

        internal void RegistrarPuntaje()
        {
            EJugador ganador = Jugador1.PuntajeActual.Puntaje > Jugador2.PuntajeActual.Puntaje ? Jugador1 : Jugador2;
            new JugadorDAL().RegistrarPuntaje(ganador);
        }

        internal bool DiferenciaAbismal()
        {
            EJugador perdiendo = Jugador1.PuntajeActual.Puntaje < Jugador2.PuntajeActual.Puntaje ? Jugador1 : Jugador2;
            EJugador ganando = perdiendo == Jugador1 ? Jugador2 : Jugador1;
            int pun = ((MAX_INT - perdiendo.PuntajeActual.Lanzamientos) * 2) + perdiendo.PuntajeActual.Puntaje;
            return pun < ganando.PuntajeActual.Puntaje;
        }
    }
}
