﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capas.Entities
{
    class EPuntaje
    {
        public int Id { get; set; }

        public int Puntaje { get; set; }

        public int Lanzamientos { get; set; }

        public DateTime Fecha { get; set; }

    }
}
