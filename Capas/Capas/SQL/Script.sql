CREATE TABLE dados.jugadores
(
    id serial NOT NULL PRIMARY KEY ,
    correo text NOT NULL unique,
    nombre text NOT NULL,
    apellidos text,
    pin integer NOT NULL,
    activo boolean NOT NULL default true
);

CREATE TABLE dados.puntajes
(
    id serial NOT NULL PRIMARY KEY ,
    id_jugador int not NULL,
    puntaje int default 0,
    lanzamientos int default 0,
    fecha timestamp default now()
);

alter table dados.puntajes add constraint fk_pun_jug FOREIGN key (id_jugador)
references dados.jugadores (id);

select* from dados.jugadores

select* from dados.puntajes