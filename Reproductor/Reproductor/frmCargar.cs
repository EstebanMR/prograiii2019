﻿using Reproductor.BOL;
using Reproductor.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reproductor
{
    public partial class frmCargar : Form
    {
        ECancion c;
        MemoryStream ms;
        private CancionBOL log;

        public frmCargar()
        {
            InitializeComponent();
            log = new CancionBOL();
            ms = new MemoryStream();
            c = new ECancion();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog files = new OpenFileDialog();
            DialogResult r = new DialogResult();
            r = files.ShowDialog();

            if (r == DialogResult.OK)
            {                
                c.Titulo = txtTitulo.Text;
                c.Autor = txtAutor.Text;
                c.Ruta = files.FileName;
                using (FileStream file = new FileStream(c.Ruta, FileMode.Open, FileAccess.Read))
                {
                    file.CopyTo(ms);
                }
                c.Cancion = ms.ToArray();
                c.Lista = (String)comboBox1.SelectedItem;
            }
            //string sb = "{ ";
            //foreach (var b in c.Cancion)
            //{
            //    sb += b + ", ";
            //}
            //sb += "}";
            //Console.WriteLine(sb.ToString());
            Close();
        }

        internal ECancion getCancion()
        {
            return c;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            txtAdd.Visible = true;

        }

        private void txtAdd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                txtAdd.Visible = false;
                comboBox1.Items.Add(txtAdd.Text);
            }
        }

        private void frmCargar_Load(object sender, EventArgs e)
        {
            List<string> listas = log.CargarCombo();
            foreach (string i in listas)
            {
                comboBox1.Items.Add(i);
            }
        }
    }
}
