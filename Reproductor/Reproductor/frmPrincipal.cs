﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Reproductor.Entities;
using Reproductor.BOL;
using System.Media;
using System.IO;

namespace Reproductor
{
    public partial class frmPrincipal : Form
    {
        private CancionBOL log;
        private List<ECancion> canciones;
        private List<string> nombres;
        private int num;

        public frmPrincipal()
        {
            InitializeComponent();
            canciones = new List<ECancion>();
            log = new CancionBOL();
            nombres = new List<string>();
            num = 0;
            Directory.CreateDirectory("C:/canciones");
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {

                frmCargar frm = new frmCargar();
                frm.ShowDialog();
                ECancion temp = frm.getCancion();
                log.Agregar(temp);
                canciones.Add(temp);
                listBox1.Items.Clear();
                num = 0;
                foreach (ECancion i in canciones)
                {
                    nombres.Add(i.Titulo);
                    listBox1.Items.Add(i.Titulo);
                    //axWindowsMediaPlayer1.
                }
                play(canciones[num]);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void play(ECancion c)
        {
            axWindowsMediaPlayer1.close();
            MemoryStream ms = new MemoryStream(c.Cancion);
            using (FileStream file = new FileStream("C:/canciones/temp.mp3", FileMode.Create, FileAccess.Write))
            {
                ms.WriteTo(file);
                c.Ruta = "C:/canciones/temp.mp3";
            }
            Console.WriteLine(c.Titulo);
            Console.WriteLine(c.Ruta);
            //PrintByteArray(c.Cancion);
            axWindowsMediaPlayer1.URL = c.Ruta;
            lblNombre.Text = c.Titulo + "(" + c.Autor + ")";
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                num = listBox1.SelectedIndex;
                play(canciones[num]);
                //PrintByteArray(canciones[num].Cancion);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void lblNombre_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                axWindowsMediaPlayer1.Ctlcontrols.pause();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            try
            {
                axWindowsMediaPlayer1.Ctlcontrols.play();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            try
            {
                if (num == 5)
                {
                    num--;
                }

                if (num >= 0)
                {
                    if (num > 0)
                    {
                        num--;
                    }
                    axWindowsMediaPlayer1.Ctlcontrols.stop();
                    Console.WriteLine(num);

                    play(canciones[num]);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                ActualizarDatosTrack();
                tbReproductor.Value = (int)axWindowsMediaPlayer1.Ctlcontrols.currentPosition;
                tbVolumen.Value = axWindowsMediaPlayer1.settings.volume;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void ActualizarDatosTrack()
        {
            if (axWindowsMediaPlayer1.playState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                tbReproductor.Maximum = (int)axWindowsMediaPlayer1.Ctlcontrols.currentItem.duration;
                timer1.Start();
            }
            else if (axWindowsMediaPlayer1.playState == WMPLib.WMPPlayState.wmppsStopped)
            {
                timer1.Stop();
            }
        }

        private void axWindowsMediaPlayer1_PlaylistChange(object sender, AxWMPLib._WMPOCXEvents_PlaylistChangeEvent e)
        {
            ActualizarDatosTrack();
            tbReproductor.Value = (int)axWindowsMediaPlayer1.Ctlcontrols.currentPosition;
            tbVolumen.Value = axWindowsMediaPlayer1.settings.volume;
        }

        private void tbVolumen_ValueChanged(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.settings.volume = tbVolumen.Value;
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            try
            {
                List<string> listas = log.CargarCombo();
                foreach (string i in listas)
                {
                    cbxAutor.Items.Add(i);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                canciones.Clear();
                List<ECancion> temp = log.CargarFiltro((string)cbxAutor.SelectedItem);
                canciones.Clear();
                listBox1.Items.Clear();
                foreach (ECancion i in temp)
                {
                    canciones.Add(i);
                    listBox1.Items.Add(i.Titulo);
                }
                num = 0;
                play(canciones[num]);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void tbReproductor_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                axWindowsMediaPlayer1.Ctlcontrols.currentPosition = Convert.ToDouble(tbReproductor.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void pictureBox6_Click(object sender, EventArgs e)
        {
            try
            {
                if (num < canciones.Count - 1)
                {
                    num++;
                    axWindowsMediaPlayer1.Ctlcontrols.stop();
                    Console.WriteLine(num);
                    axWindowsMediaPlayer1.URL = canciones[num].Ruta;
                    lblNombre.Text = canciones[num].Titulo + "(" + canciones[num].Autor + ")";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
    }
}