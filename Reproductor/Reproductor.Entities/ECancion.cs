﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reproductor.Entities
{
    public class ECancion
    {
        public int id { get; set; }
        public string Ruta { get; set; }
        public string Autor { get; set; }
        public string Lista { get; set; }
        public string Titulo { get; set; }
        public Byte[] Cancion { get; set; }
    }
}
