﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reproductor.Entities;
using Reproductor.DAL;

namespace Reproductor.BOL
{
    public class CancionBOL
    {
        public void Agregar(ECancion temp)
        {
            Conexion con = new Conexion();
            try
            {
                new CancionDAL().Agregar(temp);
               // con.Test();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public List<string> CargarCombo()
        {
            return new CancionDAL().CargarCombo();
        }

        public List<ECancion> CargarFiltro(string filtro)
        {
            return new CancionDAL().CargarFiltro(filtro);
        }
    }
}
