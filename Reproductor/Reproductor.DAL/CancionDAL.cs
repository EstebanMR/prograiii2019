﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using Reproductor.Entities;

namespace Reproductor.DAL
{
    public class CancionDAL
    {

        public void Agregar(ECancion temp)
        {
            string sql = "INSERT INTO music.canciones( titulo, autor, lista, ruta, cancion) VALUES( @tit, @aut, @lis, @rut, @can); ";
            using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                Console.WriteLine(temp.Lista);
                cmd.Parameters.AddWithValue("@tit", temp.Titulo);
                cmd.Parameters.AddWithValue("@aut", temp.Autor);
                cmd.Parameters.AddWithValue("@lis", temp.Lista);
                cmd.Parameters.AddWithValue("@rut", temp.Ruta);
                cmd.Parameters.AddWithValue("@can", temp.Cancion);
                cmd.ExecuteNonQuery();
            }
        }

        public List<ECancion> CargarFiltro(string filtro)
        {
            List<ECancion> temp = new List<ECancion>();
            string sql = "SELECT id, titulo, autor, lista, cancion, ruta FROM music.canciones WHERE lista like @lis; ";
            using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@lis", filtro);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    temp.Add(ArmarCancion(reader));
                }
            }
            return temp;
        }

        private ECancion ArmarCancion(NpgsqlDataReader reader)
        {
            ECancion temp = new ECancion
            {
                id = reader.GetInt32(0),
                Titulo = reader.GetString(1),
                Autor = reader.GetString(2),
                Lista = reader.GetString(3),
                Cancion = reader[4] != DBNull.Value ? (byte[])reader[4] : null,
                Ruta = reader.GetString(5)
            };
            return temp;
        }

        public List<string> CargarCombo()
        {
            List<string> listas = new List<string>();
            string sql = "SELECT distinct lista FROM music.canciones;";
            using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    listas.Add(reader.GetString(0));
                }
            }
            return listas;
        }
    }
}
