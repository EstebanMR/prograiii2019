﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Reproductor.DAL
{
    public class Conexion
    {
        public static string conStr = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;

        public void Test()
        {
            Console.WriteLine("probando");
            using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
            {
                conn.Open();
                Console.WriteLine("conexión exotosa");
            }
        }
    }
}
