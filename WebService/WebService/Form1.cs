﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WebService.WSBCCR;

namespace WebService
{
    public partial class Form1 : Form
    {
        double TipoCambio;
        public Form1()
        {
            InitializeComponent();
            TipoCambio = 0;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            wsindicadoreseconomicosSoapClient ws = new wsindicadoreseconomicosSoapClient("wsindicadoreseconomicosSoap");

            string xml = ws.ObtenerIndicadoresEconomicosXML("318", DateTime.Now.ToString("dd/MM/yyyy"),
                DateTime.Now.ToString("dd/MM/yyyy"), "Esteban", "N", "emurillor@est.utn.ac.cr",
                "ABRREISNRN");

            XmlDocument xD = new XmlDocument();
            xD.LoadXml(xml);

            XmlNodeList xTipo = xD.GetElementsByTagName("Datos_de_INGC011_CAT_INDICADORECONOMIC");
            XmlNodeList xLista = ((XmlElement)xTipo[0]).GetElementsByTagName("NUM_VALOR");

            TipoCambio = XmlConvert.ToDouble(xLista[0].InnerText);

            txtTipo.Text = TipoCambio.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Double valor = 0;
            if (String.IsNullOrEmpty(txtDolares.Text))
            {
                try
                {
                    valor = Convert.ToDouble(txtColones.Text) / TipoCambio;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                txtDolares.Text = valor.ToString();

            }
            else if (String.IsNullOrEmpty(txtColones.Text))
            {
                try
                {
                    valor = Convert.ToDouble(txtDolares.Text) * TipoCambio;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                txtColones.Text = valor.ToString();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtColones.Text = string.Empty;
            txtDolares.Text = string.Empty;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtDolares_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
