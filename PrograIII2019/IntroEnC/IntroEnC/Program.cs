﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace IntroEnC
{
    class Program
    {
        static void Main(string[] args)
        {
            //Clase01 cl1 = new Clase01();
            //cl1.ejemplos();
            int[] arreglo = new int[0];
            Console.WriteLine("Bienvenid@ !!");
            //Console.WriteLine();
            while (true)
            {
                int op = Menu();
                switch (op)
                {
                    case 1:
                        Suma();
                        break;
                    case 2:
                        Resta();
                        break;
                    case 3:
                        Multiplica();
                        break;
                    case 4:
                        Divide();
                        break;
                    case 5:
                        int el = Llenar();
                        arreglo = new int[el];
                        for (int i = 0; i < arreglo.Length; i++)
                        {
                            arreglo[i] = el;
                        }
                        break;
                    case 6:
                        Imprimir(arreglo);
                        break;
                    case 7:
                        Exit();
                        break;

                }
                Console.WriteLine("Presione <Enter> para continuar");
                if (Console.ReadKey().Key == ConsoleKey.Enter)
                {
                    Console.Clear();
                }

            }
            //Console.Read();
        }

        /// <summary>
        /// opción 7
        /// </summary>
        private static void Exit()
        {
            Console.WriteLine("Gracias por usar la aplicación, vuelva pronto!!");
            Environment.Exit(0);
        }
        
        /// <summary>
        /// opción 6
        /// </summary>
        /// <param name="arreglo">un arreglo a imprimir</param>
        private static void Imprimir(int[] arreglo)
        {
            Console.WriteLine("Arreglo:");
            int e = 0;
            //Console.WriteLine(arreglo);
            Console.Write("{");
            foreach (int i in arreglo)
            {
                Console.Write(" {0}", i);
                e++;
                //Console.WriteLine(e);
                if (e < arreglo.Length) 
                {
                    Console.Write(",");
                }
            }
            Console.Write(" }");
            Console.WriteLine();
        }

        /// <summary>
        /// recibe un número para llenar un array
        /// </summary>
        /// <returns>retorna número para llenar array</returns>
        private static int Llenar()
        {
            Console.WriteLine();
            Console.WriteLine("Ingrese el numero que desea ingresar al arreglo:");
            return Int32.Parse(Console.ReadLine());
            
        }

        /// <summary>
        /// pinta el menú
        /// </summary>
        /// <returns></returns>
        private static int Menu()
        {
            Console.WriteLine();
            Console.WriteLine("1. Suma\n" +
                "2. Resta\n" +
                "3. Multiplicación\n" +
                "4. División\n" +
                "5. Llenar un arreglo\n" +
                "6. Imprimir arreglo\n" +
                "7. Salir\n\n" +
                "Ingrese una opción:");
            return Int32.Parse(Console.ReadLine());
            
        }

        /// <summary>
        /// toma dos números a dividir y lo pinta 
        /// </summary>
        private static void Divide()
        {
            Operaciones op = new Operaciones();
            Console.WriteLine("Ingrese dos números para la división");
            Console.WriteLine("Ingrese el primer número:");
            int n1 = Int32.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("Ingrese el segundo número:");
            int n2 = Int32.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine(">>División: " + n1 + " / " + n2);
            double r = op.Divide(n1, n2);

            Console.WriteLine("Resultado: " + r);
            Console.WriteLine();
        }

        /// <summary>
        /// toma dos números a multiplicar y los muestra 
        /// </summary>
        private static void Multiplica()
        {
            Operaciones op = new Operaciones();
            Console.WriteLine("Ingrese dos números para la multiplicación");
            Console.WriteLine("Ingrese el primer número:");
            int n1 = Int32.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("Ingrese el segundo número:");
            int n2 = Int32.Parse(Console.ReadLine());

            Console.WriteLine(">>Multiplicación: " + n1 + " X " + n2);
            double r = op.Multiplica(n1, n2);
            Console.WriteLine();

            Console.WriteLine("Resultado: " + r);
            Console.WriteLine();
        }

        /// <summary>
        /// toma dos números a restar y lo muestra
        /// </summary>
        private static void Resta()
        {
            Operaciones op = new Operaciones();
            Console.WriteLine("Ingrese dos números para la resta");
            Console.WriteLine("Ingrese el primer número:");
            int n1 = Int32.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("Ingrese el segundo número:");
            int n2 = Int32.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine(">>Resta: " + n1 + " - " + n2);
            int r = op.Resta(n1, n2);

            Console.WriteLine("Resultado: " + r);
            Console.WriteLine();
        }

        /// <summary>
        /// toma dos números a sumar y lo muestra
        /// </summary>
        private static void Suma()
        {
            Operaciones op = new Operaciones();
            Console.WriteLine();
            Console.WriteLine("Ingrese dos números para la suma");
            Console.WriteLine("Ingrese el primer número:");
            int n1 = Int32.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("Ingrese el segundo número:");
            int n2 = Int32.Parse(Console.ReadLine());

            Console.WriteLine(">>Suma: " + n1 + " + " + n2);
            int r = op.Suma(n1, n2);

            Console.WriteLine("Resultado: " + r);
            Console.WriteLine();
        }
    }
}
