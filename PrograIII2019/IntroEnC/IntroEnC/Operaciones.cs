﻿using System;

namespace IntroEnC
{
    internal class Operaciones
    {
        /// <summary>
        /// Realiza la suma de dos números
        /// </summary>
        /// <param name="n1">número 1</param>
        /// <param name="n2">Número 2</param>
        /// <returns>retorna la suma de ambos</returns>
        internal int Suma(int n1, int n2)
        {
            return n1 + n2;
        }

        /// <summary>
        /// Raliza la resta de dos números
        /// </summary>
        /// <param name="n1">número 1</param>
        /// <param name="n2">número 2</param>
        /// <returns>retorna la resta de ambos</returns>
        internal int Resta(int n1, int n2)
        {
            return n1 - n2;
        }

        /// <summary>
        /// multiplica dos números
        /// </summary>
        /// <param name="n1">número 1</param>
        /// <param name="n2">número 2</param>
        /// <returns>retorna la multiplicación de ambos</returns>
        internal double Multiplica(int n1, int n2)
        {
            return (double)n1 * n2;
        }

        /// <summary>
        /// divide dos números
        /// </summary>
        /// <param name="n1">número 1</param>
        /// <param name="n2">número 2</param>
        /// <returns></returns>
        internal double Divide(int n1, int n2)
        {
            return (double)n1 / n2;
        }
    }
}