﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUnoEsteban
{
    class Program
    {
        static void Main(string[] args)
        {
            string menu = "Menú Práctica 1 Progra 3 UTN V-1.0\n" +
                "1. Generar saludo\n" +
                "2. El juego de tres números\n" +
                "3. Invertir números\n" +
                "4. Encontrar el mayor, el menor y el número de media en una lista de números\n" +
                "5. Maximo comun divisor de dos números\n" +
                "6. Granja de conejos\n" +
                "7. Número primo\n" +
                "8. Tiempo en descubrir los primeros 10 mil números primos\n" +
                "9. Número perfecto\n" +
                "10. Salir\n\n" +
                "Por favor, ingrese una de las opciones anteriores: \n";


            while (true)
            {
                int op = LeerInt(menu);
                switch (op)
                {
                    case 1:
                        Saludo();

                        break;

                    case 2:
                        TresNumeros();
                        break;

                    case 3:
                        Revertir();
                        break;

                    case 4:
                        Array();
                        break;

                    case 5:
                        MCD();
                        break;

                    case 6:
                        GranjaConejos();
                        break;

                    case 7:
                        Primo();
                        break;

                    case 8:
                        EncontrarPrimos();
                        break;

                    case 9:
                        Perfecto();
                        break;

                    case 10:
                        Exit();
                        break;

                    default:
                        break;
                }
                Console.WriteLine("\n");
            }

        }

        private static void Perfecto()
        {
            int n = LeerInt("Ingrese el número que desea saber si es primo: ");
            int a = 0;

            for (int i = 1; i < n + 1; i++)
            {
                if (n % i == 0)
                {
                    a += i;
                }
            }

            if (a==n)
            {
                Console.WriteLine("El número es perfecto");
            }
            else
            {
                Console.WriteLine("El número no es perfecto");
            }
        }

        private static void EncontrarPrimos()
        {

            LinkedList<int> num = new LinkedList<int>();
            Console.Write("Calculando");
            DateTime d1 = DateTime.Now;
            for (int k = 0; k < 105000; k++)
            {
                if (num.Count<10000)
                {
                    int n = k;
                    int a = 0;

                    for (int i = 1; i < n + 1; i++)
                    {
                        if (n % i == 0)
                        {
                            a++;
                        }
                    }

                    if (a == 2)
                    {
                        num.AddLast(n);
                    }
                }
                else
                {
                    break;
                }
                if (k == 10000 || k == 60000 || k == 90000)
                {
                    Console.Write(".");
                }
            }
            Console.WriteLine("");
            DateTime d2 = DateTime.Now;
            double total = d2.Subtract(d1).TotalSeconds;
            Console.WriteLine("Tiempo total : "+total+"s");
        }

        private static void Primo()
        {
            int n = LeerInt("Ingrese el número que desea saber si es primo: ");
            int a = 0;

            for (int i = 1; i < n + 1; i++)
            {
                if (n % i == 0)
                {
                    a++;
                }
            }

            if (a != 2)
            {
                Console.WriteLine("El número no es primo");
            }
            else
            {
                Console.WriteLine("El número es primo");
            }



        }

        private static void GranjaConejos()
        {
            throw new NotImplementedException();
        }

        private static void MCD()
        {
            int uno = LeerInt("#1: ");
            int dos = LeerInt("#2: ");
            int div = 0;

<<<<<<< Updated upstream
            int n1 = Math.Max(uno, dos);
            int n2 = Math.Min(uno, dos);
            div = n2 / 2;
=======
<<<<<<< Updated upstream
            if (uno < dos)
            {
                div = uno / 2;
            }
            else
            {
                div = dos / 2;
            }
=======
<<<<<<< HEAD
            int a = Math.Max(uno, dos);
            int b = Math.Min(uno, dos);
>>>>>>> Stashed changes
>>>>>>> Stashed changes


            do
            {
<<<<<<< Updated upstream
=======
<<<<<<< Updated upstream
                if (uno % div == 0 && dos % div == 0)
                {
                    break;
                }
            }
=======
                div = b;
                b = a % b;
                a = div;
            }while(b!=0);
=======
            int n1 = Math.Max(uno, dos);
            int n2 = Math.Min(uno, dos);
            div = n2 / 2;


            do
            {
>>>>>>> Stashed changes
                div = n2;
                n2 = n1 % n2;
                n1 = div;
            } while (n2!=0);
<<<<<<< Updated upstream
=======
>>>>>>> master
>>>>>>> Stashed changes
>>>>>>> Stashed changes

            Console.WriteLine("El máximo común divisor de ambos números es : " + div);

        }

        private static void Array()
        {
            int largo = LeerInt("Ingrese la cantidad de números que desea ingresar: ");
            int[] num = new int[largo];
            for (int i = 0; i < largo; i++)
            {
<<<<<<< Updated upstream
                int n = LeerInt("#" + (i + 1) + " : ");
=======
<<<<<<< Updated upstream
                int n = LeerInt("#" + i + 1 + " : ");
=======
<<<<<<< HEAD
                int pas = i + 1;
                int n = LeerInt("#" + pas + " : ");
=======
                int n = LeerInt("#" + (i + 1) + " : ");
>>>>>>> master
>>>>>>> Stashed changes
>>>>>>> Stashed changes
                num[i] = n;
            }

            int mayor = 0;
<<<<<<< Updated upstream
            int menor = num[0];
=======
<<<<<<< Updated upstream
            int menor = 0;
=======
<<<<<<< HEAD
            int menor = num[1];
=======
            int menor = num[0];
>>>>>>> master
>>>>>>> Stashed changes
>>>>>>> Stashed changes
            int sum = 0;

            for (int i = 0; i < num.Length; i++)
            {
                if (num[i] > mayor)
                {
                    mayor = num[i];
                }
                else if (num[i] < menor)
                {
                    menor = num[i];
                }
                sum += num[i];
            }
            int avg = sum / largo;
            Console.WriteLine("mayor = " + mayor + ", menor = " + menor + ", promedio = "
                + avg);
        }

        private static void Exit()
        {
            Console.WriteLine("Gracias por usar la aplicación, vuelva pronto!!");
            Environment.Exit(0);
        }

        private static void Revertir()
        {
            int nv = LeerInt("Ingrese un número de dos o más digitos: ");
            int nn = 0;
            while (nv > 0)
            {
                int residuo = nv % 10;
                nn = nn * 10 + residuo;
                nv = nv / 10;
            }
            Console.WriteLine("El número invertido es :" + nn);

        }

        private static void TresNumeros()
        {
            int uno = LeerInt("#1: ");
            int dos = LeerInt("#2: ");
            int tres = LeerInt("#3: ");
            int res = 0;
            String r = "";

            if (uno <= 0)
            {
                res = dos + tres;
                r = dos + " + " + tres + ": ";
            }
            else
            {
                res = dos * tres;
                r = dos + " X " + tres + ": ";
            }
            Console.WriteLine(r + res);

        }

        private static int LeerInt(string v)
        {
            Console.Write(v + "");
            string c = Console.ReadLine();
            int op = 0;
            if (int.TryParse(c, out op))
            {
                op = Int32.Parse(c);
            }
            else
            {
                throw new Exception("Existe un error de casteo");
            }
            Console.WriteLine("");
            return op;

        }

        private static void Saludo()
        {
            Console.Write("Por favor ingrese su nombre: ");
            string nombre = Console.ReadLine();
            string hora = System.DateTime.Now.ToString("hh:mm:ss");
            string fecha = System.DateTime.Now.ToString("dd/MM/yyyy");
            string saludo = String.Format("\nHola, bienvenido {0}, hoy es {1} a las {2}", nombre, fecha, hora);
            Console.WriteLine(saludo);
        }


    }
}
