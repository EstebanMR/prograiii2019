﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerWindowsForms
{
    public partial class frmMenus : Form
    {
        public frmMenus()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Has seleccionado la fecha: " + dateTimePicker1.Value.Date);
            MessageBox.Show("Hoy es: " + DateTime.Now);
        }

        private void NegroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label1.ForeColor = Color.Black;
            label2.ForeColor = Color.Black;
            label3.ForeColor = Color.Black;
        }

        private void AzulToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label1.ForeColor = Color.Blue;
            label2.ForeColor = Color.Blue;
            label3.ForeColor = Color.Blue;
        }

        private void RojoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label1.ForeColor = Color.Red;
            label2.ForeColor = Color.Red;
            label3.ForeColor = Color.Red;
        }

        private void VerdeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label1.ForeColor = Color.Green;
            label2.ForeColor = Color.Green;
            label3.ForeColor = Color.Green;
        }

        private void TimesNewRomanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontFamily ff = new FontFamily("Times New Roman");
            this.label1.Font = new Font(ff,
            this.label1.Font.Size, this.label1.Font.Style);
            this.label2.Font = new Font(ff,
            this.label2.Font.Size, this.label2.Font.Style);
            this.label3.Font = new Font(ff,
            this.label3.Font.Size, this.label3.Font.Style);
        }

        private void TohomaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontFamily ff = new FontFamily("Tahoma");
            this.label1.Font = new Font(ff,
            this.label1.Font.Size, this.label1.Font.Style);
            this.label2.Font = new Font(ff,
            this.label2.Font.Size, this.label2.Font.Style);
            this.label3.Font = new Font(ff,
            this.label3.Font.Size, this.label3.Font.Style);
        }

        private void ComicSansToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontFamily ff = new FontFamily("Comic Sans MS");
            this.label1.Font = new Font(ff,
            this.label1.Font.Size, this.label1.Font.Style);
            this.label2.Font = new Font(ff,
            this.label2.Font.Size, this.label2.Font.Style);
            this.label3.Font = new Font(ff,
            this.label3.Font.Size, this.label3.Font.Style);
        }

        private void NegritaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.label1.Font = new Font(this.label1.Font.FontFamily,
            this.label1.Font.Size, this.label1.Font.Style ^ FontStyle.Bold);

            this.label2.Font = new Font(this.label2.Font.FontFamily,
            this.label2.Font.Size, this.label2.Font.Style ^ FontStyle.Bold);

            this.label3.Font = new Font(this.label3.Font.FontFamily,
            this.label3.Font.Size, this.label3.Font.Style ^ FontStyle.Bold);
        }

        private void CursivaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.label1.Font = new Font(this.label1.Font.FontFamily,
            this.label1.Font.Size, this.label1.Font.Style ^ FontStyle.Italic);

            this.label2.Font = new Font(this.label2.Font.FontFamily,
            this.label2.Font.Size, this.label2.Font.Style ^ FontStyle.Italic);

            this.label3.Font = new Font(this.label3.Font.FontFamily,
            this.label3.Font.Size, this.label3.Font.Style ^ FontStyle.Italic);
        }

        private void SubrayadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.label1.Font = new Font(this.label1.Font.FontFamily,
            this.label1.Font.Size, this.label1.Font.Style ^ FontStyle.Underline);

            this.label2.Font = new Font(this.label2.Font.FontFamily,
            this.label2.Font.Size, this.label2.Font.Style ^ FontStyle.Underline);

            this.label3.Font = new Font(this.label3.Font.FontFamily,
            this.label3.Font.Size, this.label3.Font.Style ^ FontStyle.Underline);
        }

        private void NaranjaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Orange;
        }

        private void BeigeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Beige;
        }

        private void PurpuraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Purple;
        }

        private void AmarilloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Yellow;
        }

        private void RosaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Pink;
        }

        private void BlancoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.White;
        }

        private void ContactoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Creador: Esteban Murillo Rojas");
        }

        private void SalirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
