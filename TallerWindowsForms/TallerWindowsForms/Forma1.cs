﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerWindowsForms
{
    public partial class Forma1 : Form
    {

        public string Nombre { get; set; }

        public string Calle { get; set; }

        public string Colonia { get; set; }

        public string Delegacion { get; set; }

        public string CodigoPostal { get; set; }

        public string Telefono { get; set; }

        public string Correo { get; set; }

        public Forma1()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Nombre = textBox1.Text;
            Forma3 data = new Forma3();
            data.Nombre = Nombre;
            data.ShowDialog();
            Calle = data.Calle;
            Colonia = data.Colonia;
            Delegacion = data.Delegacion;
            CodigoPostal = data.CodigoPostal;
            Telefono = data.Telefono;
            Correo = data.Correo;

            label2.Text = "Provincia: " + Calle;
            label3.Text = "Cantón: " + Colonia;
            label4.Text = "Distrito: " + Delegacion;
            label5.Text = "Código Postal: " + CodigoPostal;
            label6.Text = "Teléfono: " + Telefono;
            label7.Text = "Correo: " + Correo;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Forma2 data = new Forma2();
            data.Name = Nombre;
            data.Correo = Correo;
            data.ShowDialog();
        }
    }
}
