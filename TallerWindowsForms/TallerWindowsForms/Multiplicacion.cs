﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerWindowsForms
{
    public partial class Multiplicacion : Form
    {
        public Multiplicacion()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            double d1, d2;
            if (!double.TryParse(Op1.Text, out d1))
            {
                Clear();
            }
            if (!double.TryParse(Op2.Text, out d2))
            {

                Clear();
            }
            Resultado.Text = String.Format("{0}x{1}={2:f2}",d1,d2, d1 * d2);

        }

        private void Clear()
        {
            Op1.Clear();
            Op2.Clear();
            Resultado.Clear();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }
    }
}
