﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerWindowsForms
{
    public partial class frmBuscador : Form
    {
        public frmBuscador()
        {
            InitializeComponent();
            webBrowser1.Navigate("Google.com");
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            webBrowser1.Navigate("Google.com");
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void FrmBuscador_Load(object sender, EventArgs e)
        {

        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            webBrowser1.Navigate(textBox1.Text);
        }
    }
}
