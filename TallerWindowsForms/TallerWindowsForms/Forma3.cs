﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerWindowsForms
{
    public partial class Forma3 : Form
    {


        private String Nombre1;

        public String Nombre
        {
            set { label1.Text = value; }
        }


        private string Calle1;

        public string Calle
        {
            get { return txtProv.Text; }
        }


        private string Colonia1;

        public string Colonia
        {
            get { return txtCanton.Text; }
        }


        private string Delegacion1;

        public string Delegacion
        {
            get { return txtDistrito.Text; }
        }


        private string CodigoPostal1;

        public string CodigoPostal
        {
            get { return txtCodPost.Text; }
        }


        private string Telefono1;

        public string Telefono
        {
            get { return txtTelefono.Text; }
        }

        private string correo;

        public string Correo
        {
            get { return txtCorreo.Text; }
        }

        public Forma3()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
