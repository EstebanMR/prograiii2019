﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerWindowsForms
{
    public partial class Temperaturas : Form
    {
        public Temperaturas()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            double GFareheit, GCentigrados;
            GFareheit = Convert.ToDouble(textBox1.Text);
            GCentigrados = (GFareheit - 32) / 1.8;
            textBox2.Text = String.Format("{0:F3}", GCentigrados);
        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            double GFareheit, GCentigrados;
            GCentigrados = Convert.ToDouble(textBox1.Text);
            GFareheit = (GCentigrados * 1.8) + 32;
            textBox2.Text = String.Format("{0:F3}", GFareheit);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
