﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerWindowsForms
{
    public partial class CajaSeleccion : Form
    {
        public CajaSeleccion()
        {
            InitializeComponent();
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            this.label1.Font = new Font(this.label1.Font.FontFamily ,
            this.label1.Font.Size, this.label1.Font.Style ^ FontStyle.Bold);
        }

        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            this.label1.Font = new Font(this.label1.Font.FontFamily,
            this.label1.Font.Size, this.label1.Font.Style ^ FontStyle.Italic);
        }

        private void CheckBox3_CheckedChanged(object sender, EventArgs e)
        {
            this.label1.Font = new Font(this.label1.Font.FontFamily,
            this.label1.Font.Size, this.label1.Font.Style ^ FontStyle.Strikeout);
        }

        private void CheckBox4_CheckedChanged(object sender, EventArgs e)
        {
            this.label1.Font = new Font(this.label1.Font.FontFamily,
            this.label1.Font.Size, this.label1.Font.Style ^ FontStyle.Underline);
        }

        private void CheckBox5_CheckedChanged(object sender, EventArgs e)
        {
            FontFamily ff = new FontFamily("Consolas");
            this.label1.Font = new Font(ff,
            this.label1.Font.Size, this.label1.Font.Style);
        }

        private void CheckBox6_CheckedChanged(object sender, EventArgs e)
        {
            FontFamily ff = new FontFamily("Colonna MT");
            this.label1.Font = new Font(ff,
            this.label1.Font.Size, this.label1.Font.Style);
        }

        private void CheckBox7_CheckedChanged(object sender, EventArgs e)
        {
            FontFamily ff = new FontFamily("Verdana");
            this.label1.Font = new Font(ff,
            this.label1.Font.Size, this.label1.Font.Style);
        }

        private void CheckBox8_CheckedChanged(object sender, EventArgs e)
        {
            FontFamily ff = new FontFamily("Broadway");
            this.label1.Font = new Font(ff,
            this.label1.Font.Size, this.label1.Font.Style);
        }

        private void CheckBox9_CheckedChanged(object sender, EventArgs e)
        {
            this.label1.Font = new Font(this.label1.Font.FontFamily,
            8, this.label1.Font.Style);
        }

        private void CheckBox10_CheckedChanged(object sender, EventArgs e)
        {
            this.label1.Font = new Font(this.label1.Font.FontFamily,
            12, this.label1.Font.Style);
        }

        private void CheckBox11_CheckedChanged(object sender, EventArgs e)
        {
            this.label1.Font = new Font(this.label1.Font.FontFamily,
            16, this.label1.Font.Style);
        }

        private void CheckBox12_CheckedChanged(object sender, EventArgs e)
        {
            this.label1.Font = new Font(this.label1.Font.FontFamily,
            20, this.label1.Font.Style);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            FontFamily ff = new FontFamily("Microsoft Sans Serif");
            this.label1.Font = new Font(ff, 12 , FontStyle.Regular);
        }
    }
}
