﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerWindowsForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void VentanaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void MultipicaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Multiplicacion m = new Multiplicacion();
            m.ShowDialog();
        }

        private void TemperaturasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Temperaturas t = new Temperaturas();
            t.ShowDialog();
        }

        private void MessageBoxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mensajes ms = new Mensajes();
            ms.ShowDialog();
        }

        private void SalirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CajaDeSelecciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CajaSeleccion cs = new CajaSeleccion();
            cs.ShowDialog();
        }

        private void VisorDeImagenesYEnlacesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmVisor frm = new frmVisor();
            frm.ShowDialog();
        }

        private void SalirToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void BuscadorWebToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBuscador frm = new frmBuscador();
            frm.ShowDialog();
        }

        private void LlenarFormulariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Forma1 frm1 = new Forma1();
            frm1.ShowDialog();
        }

        private void MatricesDeDatosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMatriz frm = new frmMatriz();
            frm.ShowDialog();
        }

        private void MenúsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenus frm = new frmMenus();
            frm.ShowDialog();
        }
    }
}
