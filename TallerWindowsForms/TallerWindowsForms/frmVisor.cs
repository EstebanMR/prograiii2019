﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerWindowsForms
{
    public partial class frmVisor : Form
    {
        public frmVisor()
        {
            InitializeComponent();

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
                Bitmap MyImage = new Bitmap(openFileDialog1.FileName);
                pictureBox1.Image = (Image)MyImage;
                this.Text = String.Concat("Visor de imagenes ("+openFileDialog1.FileName+")");
                Console.WriteLine(String.Concat("Visor de imagenes (" + openFileDialog1.FileName + ")"));
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel1.LinkVisited = true;
            System.Diagnostics.Process.Start("Calc");

        }

        private void LinkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel2.LinkVisited = true;
            System.Diagnostics.Process.Start("CHROME.EXE", "http:\\www.utn.ac.cr");
        }

        private void LinkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel3.LinkVisited = true;
            System.Diagnostics.Process.Start("C:\\");
        }

        private void FrmVisor_Load(object sender, EventArgs e)
        {
            //pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            //pictureBox1.Image = Image.FromFile("WhatsApp Image 2019-08-31 at 9.32.35 PM(1).jpeg");
            //this.Text = String.Concat("Visor de imagenes (" + openFileDialog1.FileName + ")");
        }
    }
}
