﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerWindowsForms
{
    public partial class Mensajes : Form
    {
        private MessageBoxButtons tipoDeBoton = MessageBoxButtons.OK;

        private MessageBoxIcon tipoDeIcono = MessageBoxIcon.Information;

        public Mensajes()
        {
            InitializeComponent();
        }

        private void ra_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void MessageBox_Load(object sender, EventArgs e)
        {

        }

        private void tipoDeBoton_CheckedChanged(object sender, EventArgs e)
        {
            if (sender == radioButton11)
            {
                tipoDeBoton = MessageBoxButtons.OK;
            }
            else if (sender == radioButton12)
            {
                tipoDeBoton = MessageBoxButtons.OKCancel;
            }
            else if (sender == radioButton13)
            {
                tipoDeBoton = MessageBoxButtons.YesNo;
            }
            else if (sender == radioButton14)
            {
                tipoDeBoton = MessageBoxButtons.YesNoCancel;
            }
            else if (sender == radioButton15)
            {
                tipoDeBoton = MessageBoxButtons.RetryCancel;
            }
            else if (sender == radioButton16)
            {
                tipoDeBoton = MessageBoxButtons.AbortRetryIgnore;
            }
        }

        private void tipoDeIcono_CheckedChanged(object sender, EventArgs e)
        {
            if (sender == radioButton17)
            {
                tipoDeIcono = MessageBoxIcon.Information;
            }
            else if (sender == radioButton18)
            {
                tipoDeIcono = MessageBoxIcon.Exclamation;

            }
            else if (sender == radioButton19)
            {
                tipoDeIcono = MessageBoxIcon.Question;
            }
            else if (sender == radioButton20)
            {
                tipoDeIcono = MessageBoxIcon.Error;
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Mensaje a desplegar", "Título de la Ventana", tipoDeBoton, tipoDeIcono);

            switch (result)
            {
                case DialogResult.OK:
                    label4.Text = "Seleccionó OK.";
                    break;

                case DialogResult.Cancel:
                    label4.Text = "Seleccionó Cancel.";
                    break;
                case DialogResult.Yes:
                    label4.Text = "Seleccionó Yes.";
                    break;
                case DialogResult.No:
                    label4.Text = "Seleccionó No.";
                    break;
                case DialogResult.Ignore:
                    label4.Text = "Seleccionó Ignore.";
                    break;
                case DialogResult.Abort:
                    label4.Text = "Seleccionó Abort.";
                    break;
                case DialogResult.Retry:
                    label4.Text = "Seleccionó Retry.";
                    break;
            }
        }
    }
}
