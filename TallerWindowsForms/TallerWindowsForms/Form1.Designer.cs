﻿namespace TallerWindowsForms
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menúToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multipicaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.temperaturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.messageBoxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cajaDeSelecciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visorDeImagenesYEnlacesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buscadorWebToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.llenarFormulariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.matricesDeDatosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menúsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(122, 20);
            this.toolStripMenuItem1.Text = "toolStripMenuItem1";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(67, 22);
            // 
            // Menu
            // 
            this.Menu.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menúToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(529, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menúToolStripMenuItem
            // 
            this.menúToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.multipicaciónToolStripMenuItem,
            this.temperaturasToolStripMenuItem,
            this.messageBoxToolStripMenuItem,
            this.cajaDeSelecciónToolStripMenuItem,
            this.visorDeImagenesYEnlacesToolStripMenuItem,
            this.buscadorWebToolStripMenuItem,
            this.llenarFormulariosToolStripMenuItem,
            this.matricesDeDatosToolStripMenuItem,
            this.menúsToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.menúToolStripMenuItem.Name = "menúToolStripMenuItem";
            this.menúToolStripMenuItem.Size = new System.Drawing.Size(68, 22);
            this.menúToolStripMenuItem.Text = "Ejercicios";
            // 
            // multipicaciónToolStripMenuItem
            // 
            this.multipicaciónToolStripMenuItem.Name = "multipicaciónToolStripMenuItem";
            this.multipicaciónToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.multipicaciónToolStripMenuItem.Text = "Multipicación";
            this.multipicaciónToolStripMenuItem.Click += new System.EventHandler(this.MultipicaciónToolStripMenuItem_Click);
            // 
            // temperaturasToolStripMenuItem
            // 
            this.temperaturasToolStripMenuItem.Name = "temperaturasToolStripMenuItem";
            this.temperaturasToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.temperaturasToolStripMenuItem.Text = "Temperaturas";
            this.temperaturasToolStripMenuItem.Click += new System.EventHandler(this.TemperaturasToolStripMenuItem_Click);
            // 
            // messageBoxToolStripMenuItem
            // 
            this.messageBoxToolStripMenuItem.Name = "messageBoxToolStripMenuItem";
            this.messageBoxToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.messageBoxToolStripMenuItem.Text = "MessageBox";
            this.messageBoxToolStripMenuItem.Click += new System.EventHandler(this.MessageBoxToolStripMenuItem_Click);
            // 
            // cajaDeSelecciónToolStripMenuItem
            // 
            this.cajaDeSelecciónToolStripMenuItem.Name = "cajaDeSelecciónToolStripMenuItem";
            this.cajaDeSelecciónToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.cajaDeSelecciónToolStripMenuItem.Text = "Caja de Selección";
            this.cajaDeSelecciónToolStripMenuItem.Click += new System.EventHandler(this.CajaDeSelecciónToolStripMenuItem_Click);
            // 
            // visorDeImagenesYEnlacesToolStripMenuItem
            // 
            this.visorDeImagenesYEnlacesToolStripMenuItem.Name = "visorDeImagenesYEnlacesToolStripMenuItem";
            this.visorDeImagenesYEnlacesToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.visorDeImagenesYEnlacesToolStripMenuItem.Text = "Visor de imagenes y enlaces";
            this.visorDeImagenesYEnlacesToolStripMenuItem.Click += new System.EventHandler(this.VisorDeImagenesYEnlacesToolStripMenuItem_Click);
            // 
            // buscadorWebToolStripMenuItem
            // 
            this.buscadorWebToolStripMenuItem.Name = "buscadorWebToolStripMenuItem";
            this.buscadorWebToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.buscadorWebToolStripMenuItem.Text = "Buscador web";
            this.buscadorWebToolStripMenuItem.Click += new System.EventHandler(this.BuscadorWebToolStripMenuItem_Click);
            // 
            // llenarFormulariosToolStripMenuItem
            // 
            this.llenarFormulariosToolStripMenuItem.Name = "llenarFormulariosToolStripMenuItem";
            this.llenarFormulariosToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.llenarFormulariosToolStripMenuItem.Text = "Llenar formularios";
            this.llenarFormulariosToolStripMenuItem.Click += new System.EventHandler(this.LlenarFormulariosToolStripMenuItem_Click);
            // 
            // matricesDeDatosToolStripMenuItem
            // 
            this.matricesDeDatosToolStripMenuItem.Name = "matricesDeDatosToolStripMenuItem";
            this.matricesDeDatosToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.matricesDeDatosToolStripMenuItem.Text = "Matrices de datos";
            this.matricesDeDatosToolStripMenuItem.Click += new System.EventHandler(this.MatricesDeDatosToolStripMenuItem_Click);
            // 
            // menúsToolStripMenuItem
            // 
            this.menúsToolStripMenuItem.Name = "menúsToolStripMenuItem";
            this.menúsToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.menúsToolStripMenuItem.Text = "Menús";
            this.menúsToolStripMenuItem.Click += new System.EventHandler(this.MenúsToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(221, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.SalirToolStripMenuItem_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(229, 138);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Bienvenido";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 308);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ContextMenuStrip Menu;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menúToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multipicaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem temperaturasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem messageBoxToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem cajaDeSelecciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visorDeImagenesYEnlacesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buscadorWebToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem llenarFormulariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem matricesDeDatosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menúsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
    }
}

