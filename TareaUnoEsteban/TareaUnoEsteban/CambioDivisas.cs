﻿namespace TareaUnoEsteban
{
    internal class CambioDivisas
    {
        public double colones{ get; set; }

        public double tipoCambio { get; set; }

        public CambioDivisas(double col)
        {
            colones = col;
        }

        public double ConvertirADolares()
        {
            return (colones/tipoCambio);
        }
    }
}