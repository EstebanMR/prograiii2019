﻿namespace TareaUnoEsteban
{
    internal class Articulo
    {
        public int clave { get; set; }

        public string descripcion { get; set; }

        public double precio { get; set; }

        public int cantidad { get; set; }

        public Articulo(int c, string d, double p, int ca)
        {
            clave = c;
            descripcion = d;
            precio = p;
            cantidad = ca;
        }

        public double CalcularIva()
        {
            return (precio*0.13);
        }
    }
}