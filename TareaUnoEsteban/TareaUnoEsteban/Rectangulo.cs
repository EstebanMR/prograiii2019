﻿namespace TareaUnoEsteban
{
    internal class Rectangulo
    {
        public double ancho { get; set; }

        public double largo { get; set; }

        public double CalcularArea()
        {
            return (largo * ancho);
        }
    }
}