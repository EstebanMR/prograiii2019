﻿using System;

namespace TareaUnoEsteban
{
    internal class Circunferencia
    {
        public double Radio { get; set; }

        public double CalcularArea()
        {
            return (Math.PI * Math.Pow(Radio, 2));
        }

        public double CarlcularPerimetro()
        {
            return (2 * Math.PI * Radio);
        }
    }
}