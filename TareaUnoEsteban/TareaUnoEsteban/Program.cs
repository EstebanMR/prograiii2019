﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TareaUnoEsteban
{
    class Program
    {
        static int LeerInt(String msj)
        {
            int n = 0;

        APP:
            Console.Write(msj + "");
            string op = Console.ReadLine();
            if (!Int32.TryParse(op, out n))
            {
                goto APP;
            }

            return n;
        }
        static void Main(string[] args)
        {
            Logica log = new Logica();
            Rectangulo r = new Rectangulo();
            Fecha f = new Fecha();
            Fecha f1 = new Fecha(24, 9, 2019);
            string menu = "Menú tarea 1 UTN V1.0\n\n" +
                "1. Ejercicios 1 y 2\n" +
                "2. Ejercios 3 y 4\n" +
                "3. Ejercicios 5\n" +
                "4. Ejercicio 6\n" +
                "5. Ejercicio 7\n" +
                "6. Ejercicio 8\n" +
                "7. Ejercicio 9\n" +
                "8. Ejercicio 10\n" +
                "9. Ejercicios 11\n" +
                "10. Salir\n\n" +
                "Ingrese una opción:";
            Circunferencia moneda = new Circunferencia();
            moneda.Radio = 1.4;

            Circunferencia rueda = new Circunferencia { Radio = 10.2 };

            while (true)
            {
                int op = LeerInt(menu);
                Console.WriteLine();
                switch (op)
                {
                    case 1:
                        Console.WriteLine("A continuación se calculará el area y el perímetro de una rueda de radio 10.2 y una moneda de radio 1.4.");
                        Console.WriteLine("El área de la moneda es de {0}", moneda.CalcularArea());
                        Console.WriteLine("El área de la rueda es de {0}", rueda.CalcularArea());
                        Console.WriteLine("El perímetro de la moneda es de {0}", moneda.CarlcularPerimetro());
                        Console.WriteLine("El perímetro de la rueda es de {0}", rueda.CarlcularPerimetro());
                        break;

                    case 2:
                        Console.WriteLine("A continuación se muestra una pared y una ventana de forma rectangular donde usted deberá ingresar \nlas dimensiones (largo y ancho) y el programa calculará el tiempo que durará en pintarse, si en 1m^2 dura 10 min, basandose en el área.");
                        int lp = LeerInt("Ingrese el largo de la pared: ");
                        int ap = LeerInt("Ingrese el ancho de la pared: ");
                        int lv = LeerInt("Ingrese el largo de la ventana: ");
                        int av = LeerInt("Ingrese el ancho de la ventana: ");
                        r.largo = lp;
                        r.ancho = ap;
                        double areap = r.CalcularArea();
                        r.largo = lv;
                        r.ancho = av;
                        double areav = r.CalcularArea();
                        double areat = log.AreaTotal(areap, areav);
                        Console.WriteLine("El tiempo que durará en pintarse la pared es de {0}m", areat);
                        break;
                    case 3:
                        Console.WriteLine(f.FechaConLetra());
                        Console.WriteLine(f1.FechaConLetra());
                        break;
                    case 4:
                        int c = LeerInt("Ingrese la clave del producto: ");
                        Console.Write("Ingrese una pequeña descripción: ");
                        string d = Console.ReadLine();
                        Console.Write("\nIngrese el precio:");
                        double p = Double.Parse(Console.ReadLine());
                        int ca = LeerInt("Ingrese la cantidad del producto: ");
                        Articulo ar = new Articulo(c, d, p, ca);
                        Console.WriteLine(ar.CalcularIva());
                        break;
                    case 5:
                        Console.Write("Ingrese la temperatura en grados centígados: ");
                        Temperatura t = new Temperatura(double.Parse(Console.ReadLine()));
                        Console.WriteLine("La temperatura es {0}°F", t.GradosFarenheit());
                        break;
                    case 6:
                        Console.Write("Ingrese la cantidad en colones: ");
                        double col = double.Parse(Console.ReadLine());
                        CambioDivisas cd = new CambioDivisas(col);
                        cd.tipoCambio = 585.0;
                        Console.Write(" = ${0}", cd.ConvertirADolares()) ;
                        break;
                    case 7:
                            
                        break;
                    case 8:

                        break;
                    case 10:
                        log.Salir();
                        break;
                    default:
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            }
        }
    }
}
