﻿using System;

namespace TareaUnoEsteban
{
    internal class Temperatura
    {
        public double GradosCentigrados { get; set; }

        public Temperatura(double GradosCentigrados)
        {
            this.GradosCentigrados = GradosCentigrados;
        }

        internal double GradosFarenheit()
        {
            return ((GradosCentigrados*(9/5))+32);
        }
    }
}