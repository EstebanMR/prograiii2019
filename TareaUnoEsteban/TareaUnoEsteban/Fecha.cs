﻿using System;

namespace TareaUnoEsteban
{
    public class Fecha
    {
        public int dia { get; set; }

        public int mes { get; set; }

        public int anno { get; set; }

        public Fecha()
        {
            dia = System.DateTime.Now.Day;
            mes = System.DateTime.Now.Month;
            anno = System.DateTime.Now.Year;
        }

        public Fecha(int dia, int mes, int anno)
        {
            if (!verifica(dia, mes, anno))
            {
                throw new Exception("Error en los datos de la fecha");
            }
            else
            {
                this.dia = dia;
                this.mes = mes;
                this.anno = anno;

            }
        }

        public void ModificarFecha(int dia, int mes, int anno)
        {
            if (!verifica(dia, mes, anno))
            {
                throw new Exception("Error en los datos de la fecha");
            }
            else
            {
                this.dia = dia;
                this.mes = mes;
                this.anno = anno;

            }
        }

        private bool verifica(int dia, int mes, int anno)
        {
            if (dia < 32 && dia > 0 && mes > 0 && mes < 13)
            {
                return true;
            }
            return false;
        }

        public String FechaConLetra()
        {
            String mess = "";
            switch (this.mes)
            {
                case 1:
                    mess = "enero";
                    break;
                case 2:
                    mess = "febrero";
                    break;
                case 3:
                    mess = "marzo";
                    break;
                case 4:
                    mess = "abril";
                    break;
                case 5:
                    mess = "mayo";
                    break;
                case 6:
                    mess = "junio";
                    break;
                case 7:
                    mess = "julio";
                        break;
                case 8:
                    mess = "agosto";
                    break;
                case 9:
                    mess = "setiembre";
                    break;
                case 10:
                    mess = "octubre";
                    break;
                case 11:
                    mess = "noviembre";
                    break;
                case 12:
                    mess = "diciembre";
                    break;
                default:
                    break;
            }
            String f = String.Format("{0} de {1} del {2}", this.dia, mess, this.anno);
            return f;
        }
    }
}