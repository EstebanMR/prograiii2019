﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Navegador
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            webBrowser1.GoHome();
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            webBrowser1.Navigate(textBox1.Text);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            webBrowser1.GoHome();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }
    }
}
